# ExercicioFixacao01
Escrevam uma função em Javascript que retorna um número aleatório entre 1 e 10 depois de 3 segundos. 
A função deve retornar uma Promessa que, caso o número aleatório seja menor que 5, retorna sucesso. 
Se o número for maior que 5, retorna erro.

## Salvando curiosidade
```JavaScript
(async () => {
    const valor = await randomNumber();
    console.log(valor)
})()
```