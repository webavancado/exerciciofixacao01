function randomNumber(){
    return new Promise(function(myResolve, myReject) {
        setTimeout(function(){ 
            const rNumber = Math.floor(Math.random() * 10) + 1
            if(rNumber < 5){ myResolve(rNumber); }
            else{ myReject("Erro! numero maior que 5"); } 
        }, 3000);
      }); 
    }